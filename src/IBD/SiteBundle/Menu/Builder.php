<?php

namespace IBD\SiteBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{

    public function mainMenu(FactoryInterface $factory, array $options)
    {

        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav'));

        // Top Level Navigation Items
        $menu->addChild('Home', array('route' => 'home', 'attributes' => array('class' => 'colapse-menu')));
	    $menu->addChild('About', array('route' => 'about', 'attributes' => array('class' => 'colapse-menu')));
//        $menu->addChild('Tools', array('route' => '_tools', 'attributes' => array('class' => 'menu-item menu-item-type-post_type menu-item-object-page noo-nav-item dropdown mega mega-align-right')))->setAttribute('dropdown', true)->setAttribute('mega_menu', true);
	    $menu->addChild('Contact', array('route' => 'contact', 'attributes' => array('class' => 'colapse-menu')));


	    // SubNavigation Items
	    // About Us SubNav
//	    $menu['About']->addChild('Pricing Advantage', array('route' => '_pricing_advantage'));
//	    $menu['About']->addChild('Trading Technology', array('route' => '_trading_technology'));
//	    $menu['About']->addChild('Financial Security', array('route' => '_financial_security'));
//	    $menu['About']->addChild('Forex', array('route' => '_forex'))->setAttribute('class', 'menu-item menu-item-type-custom menu-item-object-custom menu-item-6549 noo-nav-item dropdown-submenu mega mega-align-right')->setAttribute('dropdown', true)->setAttribute('subnav', true);

		// Tools SubNav
//		$menu['Tools']->addChild('xFinancial', array('route' => '_xfinancial', 'attributes' => array('class' => 'anchor_link') ));
//		$menu['Tools']->addChild('xStation', array('route' => '_xstation', 'attributes' => array('class' => 'anchor_link') ));
//		$menu['Tools']->addChild('xSocial', array('route' => '_xsocial', 'attributes' => array('class' => 'anchor_link') ));
//		$menu['Tools']->addChild('xTablet', array('route' => '_xtablet', 'attributes' => array('class' => 'anchor_link') ));
//		$menu['Tools']->addChild('xMobile', array('route' => '_xmobile', 'attributes' => array('class' => 'anchor_link') ));

	    // Support SubNav
//	    $menu['Support']->addChild('Contact Us', array('route' => '_support'));
//	    $menu['Support']->addChild('Frequently Asked Questions', array('route' => '_frequently_asked_questions'));
//	    $menu['Support']->addChild('Forms & Agreements', array('route' => '_forms_agreements'));

		// Flyout SubNavigation Items
		// About > Forex Flyout SubNav
//		$menu['About']['Forex']->addChild('Leverage', array('route' => '_leverage'));
//		$menu['About']['Forex']->addChild('Making and Losing Money', array('route' => '_making_and_losing_money'));
//		$menu['About']['Forex']->addChild('Buying and Selling', array('route' => '_buying_and_selling'));
//		$menu['About']['Forex']->addChild('The Risks', array('route' => '_the_risks'));
//		$menu['About']['Forex']->addChild('Currency Quotes', array('route' => '_currency_quotes'));
//		$menu['About']['Forex']->addChild('Most Tradable Currency Pairs', array('route' => '_most_tradeable_currency_pairs'));
//		$menu['About']['Forex']->addChild('What Moves a Currency?', array('route' => '_what_moves_a_currency'));
//		$menu['About']['Forex']->addChild('Hedging', array('route' => '_hedging'));


        return $menu;

    }

}