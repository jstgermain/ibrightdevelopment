<?php

namespace IBD\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use IBD\SiteBundle\Entity\Contact;
use IBD\SiteBundle\Form\ContactType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
	public function contactAction(Request $request)
	{

		//Create contact form entity instance
		$contact = new Contact();
		$contactForm = $this->createForm(new ContactType(), $contact);

		if ($request->isMethod('POST')) {

			//Bind the posted data to the form
			$contactForm->bind($request);

			if ($contactForm->isValid()) {

				//Get the entity manager and persist the subscribe
				$em = $this->getDoctrine()->getManager();
				$em->persist($contact);
				$em->flush();

				//Redirect the user and add a thank you message
				$message = \Swift_Message::newInstance()
					->setSubject('iBrightDevelopment - Contact Form')
					->setTo($this->container->getParameter('send_emails_to'))
					->setFrom(array($this->container->getParameter('swiftmailer.sender_address') => 'iBrightDevelopment'))
					->setBody(
						$this->renderView(
							'SiteBundle:Mail:contact.html.twig',
							array(
								'ip' => $request->getClientIp(),
								'name' => $contactForm->get('name')->getData(),
								'phone' => $contactForm->get('phone')->getData(),
								'email' => $contactForm->get('email')->getData(),
								'message' => $contactForm->get('message')->getData()
							)
						)
					);

				$this->get('mailer')->send($message);

				$request->getSession()->getFlashBag()->add('success', 'Your message has been received. Someone will be in touch with you shortly.');

			} else {

				$request->getSession()->getFlashBag()->add('error', 'Uh oh! You broke our contact form. Please try again later.');

			}

		}

		return $this->render('SiteBundle:Contact:default.html.twig', array(
			'contact' => $contactForm->createView(),
		));

	}

}
