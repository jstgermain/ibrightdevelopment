<?php

namespace IBD\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TutorialController extends Controller
{
    public function tutorialAction()
    {
        return $this->render('SiteBundle:Tutorial:default.html.twig', array(
                // ...
            ));    }

}
