<?php

namespace IBD\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('name', 'text', array(
		        'attr' => array(
			        'placeholder'   => 'Name',
			        'pattern'       => '.{2,}', //minlength
		        )
	        ))
	        ->add('email', 'email', array(
		        'attr' => array(
			        'placeholder'   => 'E-mail'
		        )
	        ))
	        ->add('phone', 'text', array(
		        'attr' => array(
			        'placeholder'   => 'Phone',
			        'pattern'       => '.{2,}', //minlength
		        )
	        ))
	        ->add('message', 'textarea', array(
		        'attr' => array(
			        'cols' => 90,
			        'rows' => 10,
			        'placeholder' => 'Message'
		        )
	        ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IBD\SiteBundle\Entity\Contact'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ibd_sitebundle_contact';
    }
}
